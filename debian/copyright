Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: opensbi
Source: https://github.com/riscv/opensbi

Files: *
Copyright: 2019-2020 Western Digital Corporation or its affiliates and
 other contributors.
 2019-2021 Western Digital Corporation or its affiliates.
 2021 Christoph Müllner <cmuellner@linux.com>
 2021 YADRO
 2021 Cobham Gaisler AB.
 2021 Gabriel Somlo
 2021 SiFive
 2021 Samuel Holland <samuel@sholland.org>
License: BSD-2-clause

Files: include/sbi_utils/sys/htif.h
  lib/utils/sys/htif.c
Copyright: 2010-2020, The Regents of the University of California
License: BSD-3-clause

Files: debian/*
Copyright: 2019-2021 Vagrant Cascadian <vagrant@debian.org>
License: BSD-2-clause

Files: platform/fpga/ariane/*
Copyright: 2019 FORTH-ICS/CARV
License: BSD-2-clause

Files: platform/andes/*
Copyright: 2019 Andes Technology Corporation
License: BSD-2-clause

Files:     lib/utils/libfdt/*
Copyright: 2006-2012 David Gibson, IBM Corporation.
           2012 Kim Phillips, Freescale Semiconductor.
           2014 David Gibson <david@gibson.dropbear.id.au>
           2016 Free Electrons
           2016 NextThing Co.
           2018 embedded brains GmbH
License:   BSD-2-clause or GPL-2+

Files: lib/utils/libfdt/objects.mk
Copyright: 2019 Western Digital Corporation or its affiliates.
License: BSD-2-clause

Files:
 lib/utils/libquad/divdi3.c
 lib/utils/libquad/moddi3.c
 lib/utils/libquad/qdivrem.c
 lib/utils/libquad/quad.h
 lib/utils/libquad/udivdi3.c
 lib/utils/libquad/umoddi3.c
Copyright:
 1992, 1993 The Regents of the University of California.
License: BSD-3-clause

Files:
 lib/utils/libquad/include/limits.h
 lib/utils/libquad/include/sys/cdefs.h
 lib/utils/libquad/include/sys/types.h
 lib/utils/libquad/objects.mk
Copyright: 2021 Jessica Clarke <jrtc27@jrtc27.com>
License: BSD-2-clause

Files:
 include/sbi_utils/fdt/*
 lib/utils/fdt/*
Copyright:
 2020 Bin Meng <bmeng.cn@gmail.com>
 2021 Western Digital Corporation or its affiliates.
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public
 License Version 2.0 can be found in
 `/usr/share/common-licenses/GPL-2.0'.
